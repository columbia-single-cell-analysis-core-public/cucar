######## -----------------------------------------------------------------------
########                 Private Functions
######## ---------------------------------------------------------------

###### -------------------------------------
######     .get_overlay_plot_aesthetics

.get_overlay_plot_aesthetics = function(num_row) {
  
  # get aestetics
  stroke_size = 1
  if (num_row > 500) { stroke_size = 0.5 }
  if (num_row > 1000) { stroke_size = 0.1 }
  if (num_row > 2000) { stroke_size = 0.05 }
  if (num_row > 3000) { stroke_size = 0.01 }
  
  alpha_val = 0.8
  if (num_row > 200) { alpha_val = 0.5 }
  if (num_row > 600) { alpha_val = 0.2 }
  if (num_row > 1600) { alpha_val = 0.08 }
  if (num_row > 3200) { alpha_val = 0.04 }
  
  return(list(stroke_size=stroke_size, alpha_val=alpha_val))
}

######## -----------------------------------------------------------------------
########                 Public Functions
######## ---------------------------------------------------------------

###### --------------------------------------
######     get_cluster_overlay_plot

get_cluster_overlay_plot = function(two_dimensional_space, cluster) {
  
  # check input
  stopifnot(class(two_dimensional_space)=='data.frame')
  stopifnot(class(cluster)=='data.frame')
  stopifnot(ncol(cluster)==1)
  stopifnot(all(rownames(two_dimensional_space)==rownames(cluster)))
  
  # create data for the plot
  pdata = data.frame(two_dimensional_space, cluster, check.names = F)
  
  # get aesthetics
  aes_list = .get_overlay_plot_aesthetics(nrow(pdata))
  
  # get ordered clusters
  clusters = as.character(unique(pdata[,3]))
  if (!any(suppressWarnings(is.na(as.numeric(clusters))))) { clusters = clusters[order(as.numeric(clusters))] }
  
  # compute the cluster "centers"
  cluster_centers = t(sapply(clusters, function(c, two_dimensional_space, cluster) {
    
    row = sapply(colnames(two_dimensional_space), function(d) {
      return(median(two_dimensional_space[cluster[,1]==c,d]))
    })
    
    return(row)
  }, two_dimensional_space, cluster))
  cluster_centers = data.frame(cluster_centers, cluster=rownames(cluster_centers), check.names = F)
  
  # make the cluster plot
  cluster_plot = ggplot2::ggplot(pdata, ggplot2::aes(x=get(colnames(pdata)[1]),
                                                     y=get(colnames(pdata)[2]),
                                                     color=get(colnames(pdata)[3])))
  cluster_plot = cluster_plot + ggplot2::geom_point(ggplot2::aes(stroke=aes_list$stroke_size,
                                                                 alpha=aes_list$alpha_val))
  cluster_plot = cluster_plot + ggplot2::scale_alpha(guide=F)
  cluster_plot = cluster_plot + ggplot2::scale_color_discrete(breaks=clusters)
  cluster_plot = cluster_plot + ggplot2::theme_minimal()
  cluster_plot = cluster_plot + ggplot2::theme(legend.title = ggplot2::element_blank())
  cluster_plot = cluster_plot + ggplot2::xlab(colnames(pdata)[1])
  cluster_plot = cluster_plot + ggplot2::ylab(colnames(pdata)[2])
  cluster_plot = cluster_plot + ggplot2::ggtitle(colnames(pdata)[3])
  cluster_plot = cluster_plot + ggplot2::geom_text(data = cluster_centers,
                                                   mapping = ggplot2::aes_string(x=colnames(cluster_centers)[1],
                                                                                 y=colnames(cluster_centers)[2],
                                                                                 label=colnames(cluster_centers)[3]),
                                                   inherit.aes = F)
  
  return(cluster_plot)
}


get_overlay_plot = function(two_dimensional_space, overlay_data, cluster=NULL) {
  
  # check input
  stopifnot(class(two_dimensional_space)=='data.frame')
  stopifnot(class(overlay_data)=='data.frame')
  stopifnot(ncol(overlay_data)==1)
  stopifnot(all(rownames(two_dimensional_space)==rownames(overlay_data)))
  
  if (!is.null(cluster)) {
    stopifnot(class(cluster)=='data.frame')
    stopifnot(ncol(cluster)==1)
    stopifnot(all(rownames(two_dimensional_space)==rownames(cluster)))
  }
  
  # create data for the plot
  pdata = data.frame(two_dimensional_space, overlay_data, check.names = F)
  
  # get aesthetics
  aes_list = .get_overlay_plot_aesthetics(nrow(pdata))
  
  # set the plot scale if we have a numeric overlay varible
  if (is(pdata[,3], 'numeric')) {
    
    color_midpoint = NULL
    if (!is.null(cluster)) {
      
      color_midpoint = mean(sapply(unique(cluster[,1]), function(x) {
        return(median(pdata[cluster[,1]==x,3]))
      }))
      
    } else {
      color_midpoint = median(pdata[,3])
    }
    
    # curb outliers
    val_sd = sd(pdata[,3])
    pdata[pdata[,3] > color_midpoint + (val_sd * 3), 3] = color_midpoint + (val_sd * 3)
    pdata[pdata[,3] < color_midpoint - (val_sd * 3), 3] = color_midpoint - (val_sd * 3)
  }
  
  # create the plot
  overlay_plot = ggplot2::ggplot(pdata, ggplot2::aes(x=get(colnames(pdata)[1]),
                                                     y=get(colnames(pdata)[2]),
                                                     color=get(colnames(pdata)[3])))
  overlay_plot = overlay_plot + ggplot2::geom_point(ggplot2::aes(stroke=aes_list$stroke_size,
                                                                 alpha=aes_list$alpha_val))
  if (is(pdata[,3], 'numeric')) {
    overlay_plot = overlay_plot + ggplot2::scale_color_gradient2(low = 'blue',
                                                                 mid = 'grey',
                                                                 high = 'red',
                                                                 midpoint = color_midpoint)
  }
  overlay_plot = overlay_plot + ggplot2::scale_alpha(guide=F)
  overlay_plot = overlay_plot + ggplot2::theme_minimal()
  overlay_plot = overlay_plot + ggplot2::theme(legend.title = ggplot2::element_blank())
  overlay_plot = overlay_plot + ggplot2::xlab(colnames(pdata)[1])
  overlay_plot = overlay_plot + ggplot2::ylab(colnames(pdata)[2])
  overlay_plot = overlay_plot + ggplot2::ggtitle(colnames(pdata)[3])
  
  return(overlay_plot)
}
