######## -----------------------------------------------------------------------
########                 Public Functions
######## ---------------------------------------------------------------

###### -------------------------------------
######     vstc_get_parameters

vstc_get_parameters = function(
  latent_var=c('log_umi'),
  batch_var=NULL,
  latent_var_nonreg=NULL,
  n_genes=2000,
  n_cells=NULL,
  method='qpoisson',
  bin_size=500,
  min_cells=5,
  residual_type='pearson',
  return_cell_attr=FALSE,
  return_gene_attr=FALSE,
  return_corrected_umi=TRUE,
  verbosity=2
) {
  
  parameters = list(
    latent_var=latent_var,
    batch_var=batch_var,
    latent_var_nonreg=latent_var_nonreg,
    n_genes=n_genes,
    n_cells=n_cells,
    method=method,
    bin_size=bin_size,
    min_cells=min_cells,
    residual_type=residual_type,
    return_cell_attr=return_cell_attr,
    return_gene_attr=return_gene_attr,
    return_corrected_umi=return_corrected_umi,
    verbosity=verbosity
  )
  
  return(parameters)
}

###### -------------------------------------
######     get_vstc

get_vstc = function(counts, metadata=NULL, parameters=vstc_get_parameters()) {
  
  return(
    sctransform::vst(
      umi = counts,
      cell_attr = metadata,
      latent_var = parameters$latent_var,
      batch_var = parameters$batch_var,
      latent_var_nonreg = parameters$latent_var_nonreg,
      n_genes = parameters$n_genes,
      n_cells = parameters$n_cells,
      method = parameters$method,
      bin_size = parameters$bin_size,
      min_cells = parameters$min_cells,
      residual_type = parameters$residual_type,
      return_cell_attr = parameters$return_cell_attr,
      return_gene_attr = parameters$return_gene_attr,
      return_corrected_umi = parameters$return_corrected_umi,
      verbosity = parameters$verbosity
    )
  )
}

