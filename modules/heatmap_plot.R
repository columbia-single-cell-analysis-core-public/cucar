######## -----------------------------------------------------------------------
########                 Public Functions
######## ---------------------------------------------------------------

###### --------------------------------------
######     get_heatmap_plot

get_heatmap_plot = function(expression_data, group, genes, zlimit=2) {
  
  # check input
  stopifnot(is(expression_data, 'Matrix'))
  stopifnot(is(group, 'data.frame'))
  stopifnot(ncol(group)==1)
  stopifnot(is(genes, 'character'))
  stopifnot(all(colnames(expression_data)==rownames(group)))
  stopifnot(all(genes %in% rownames(expression_data)))
  
  # clean the groups
  group = droplevels(group)
  
  # get the plotting data
  expression_data = t(as.matrix(expression_data[genes,,drop=F]))
  expression_data = scale(expression_data)
  
  pdata = data.frame(cell_name=rownames(expression_data),
                     group,
                     expression_data,
                     check.names = F)
  
  # make the heatmap
  
  # get gene order
  hc = hclust(dist(t(pdata[,genes,drop=F])))
  gene_order = hc$labels[hc$order]
  
  # get group order
  group_data = sapply(unique(as.character(group[,1])), function(g) {
    return(apply(pdata[pdata[,colnames(group)]==g,genes], 2, mean))
  })
  hc = hclust(dist(t(group_data)))
  group_order = hc$labels[hc$order]
  
  # order the group factor
  pdata[,colnames(group)] = factor(pdata[,colnames(group)], levels=group_order)
  
  # melt the data
  pmelt = reshape2::melt(pdata,
                         id.vars = c('cell_name', colnames(pdata)[2]),
                         variable.name = 'gene',
                         value.name = 'ez_score')
  pmelt[pmelt[,'ez_score'] > zlimit,'ez_score'] = zlimit
  pmelt[pmelt[,'ez_score'] < -zlimit,'ez_score'] = -zlimit
  
  gs_heatmap = ggplot2::ggplot(pmelt, ggplot2::aes_string(y='gene', x='cell_name'))
  gs_heatmap = gs_heatmap + ggplot2::geom_tile(ggplot2::aes_string(fill = 'ez_score'))
  gs_heatmap = gs_heatmap + ggplot2::scale_fill_gradientn(
    colors = c('blue2','blue','white','red','red2'), name = 'Exp Z-Score', limits = c(-zlimit,zlimit)
  )
  gs_heatmap = gs_heatmap + ggplot2::ylab('gene') + ggplot2::xlab(colnames(group)[1])
  gs_heatmap = gs_heatmap + ggplot2::scale_y_discrete(limits = gene_order)
  gs_heatmap = gs_heatmap + ggplot2::theme_minimal()
  gs_heatmap = gs_heatmap + ggplot2::theme(axis.text.x = ggplot2::element_blank(),
                                           axis.ticks.x = ggplot2::element_blank(),
                                           strip.placement = 'outside',
                                           panel.spacing.x = ggplot2::unit(x = .1, units = 'picas'),
                                           panel.grid = ggplot2::element_blank())
  gs_heatmap = gs_heatmap + ggplot2::facet_grid(. ~ get(colnames(pdata)[2]), scales = 'free_x', space = 'fixed', switch = 'x')
  
  return(gs_heatmap)
}
